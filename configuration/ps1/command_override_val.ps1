Param
(
    [String]$Params
)

. "$PSScriptRoot\Function.ps1"
$Params = $Params.trim()
if($Params.StartsWith('"')) {
    $Params = $Params.Substring(1)
}
if($Params.EndsWith('"')) {
    $Params = $Params.Substring(0, $Params.length -1)
}
Add-Vars $Params