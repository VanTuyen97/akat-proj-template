. "$PSScriptRoot\Function.ps1"

# *** CONTANST
$currentDir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
$currentDir = Get-Item $currentDir
$rootProj = $currentDir.Parent.Parent.FullName
# # $Path
$akatPath = [string](where.exe aka-rendezvous-1.0.1.jar)
$akatPath = $akatPath.Replace(" ", "`` ")
$akatDir = $akatPath -replace "aka-rendezvous-1.0.1.jar",""
$jdkPath = $akatDir + "jdk"
# Copy Capability
Copy-Capability "$rootProj\configuration\cap" "$rootProj\capabilities"
# Delete the built directory of the Language server
$javaBuild = Join-Path -Path $env:TEMP -ChildPath "java_ws"
Remove-Item -Path $javaBuild -Recurse -Force -Confirm:$false | Out-Null

$banner = 'type "' + $currentDir.Parent.FullName + '\banner.txt"'
Invoke-Expression $banner
# Get environment
$envVals = $rootProj + "\env.properties"
$env = Get-Variable $envVals
# Run if spcify test file path
if($Null -ne $env.tartgetPath -and $env.tartgetPath -ne "") {
    $absPath = $rootProj + "\" + $env.tartgetPath
    $testFiles = Get-File-Test $absPath
    $testFiles | Foreach-Object {
        if ($_ -ne $null) {
            # Free sources
            Close-Resources $jdkPath
            $fileId = Get-FileID $_
            Write-Output ''
            Write-Output '##############################################################################'
            Write-Output '#######-RUN-##################################################################'
            Write-Output '##############################################################################'
            $msg = '############file path: ' + $_.replace($rootProj,"") + '############'
            Write-Output $msg
            $msg = '############file Id  : ' + $fileId + '###################'
            Write-Output $msg
            Write-Output '##############################################################################'
            Write-Output '##############################################################################'
            $command = $akatDir + 'jdk\bin\java -jar "' + $akatPath + '" --headless --account "' + $env.akamanager_username + '" --password "' + $env.akamanager_password + '" --project "' + $rootProj + '" --testId "' + $fileId + '" 2>&1'
            Invoke-Expression $command | Foreach-Object {
                if(
                    !($_ -match "server/logs") -and
                    !($_ -match "GenericEndpoint notify") -and
                    !($_ -match "execute/progress") -and
                    !($_ -match "ng.exe") -and
                    !($_ -match "Driver Service") -and
                    !($_ -match "No element found with strategy") -and
                    $_ -match "(^\s+at|^\s+\.\.\.|^[^\s]|^\s+message =|^\s+type =|^\s+<|^\s+\+)" -and
                    !($_ -match "Start class")
                ) {
                    Write-Output $_
                }
            }
            # Free sources
            Close-Resources $jdkPath
        }
    }
}
# Run if has testId
if($Null -ne $env.tartgetId -and $env.tartgetId -ne "") {
    # Free sources
    Close-Resources $jdkPath
    Write-Output '##############################################################################'
    Write-Output '#######-RUN-##################################################################'
    Write-Output '##############################################################################'
    Write-Output $msg   
    $msg = '############file Id: ' + $env.tartgetId + '###################'
    Write-Output $msg
    Write-Output '##############################################################################'
    Write-Output '##############################################################################'
    $command = $akatDir + 'jdk\bin\java -jar "' + $akatPath + '" --headless --account "' + $env.akamanager_username + '" --password "' + $env.akamanager_password + '" --project "' + $rootProj + '" --testId "' + $env.tartgetId + '" 2>&1'
    Invoke-Expression $command | Foreach-Object {
        if(
            !($_ -match "server/logs") -and
            !($_ -match "GenericEndpoint notify") -and
            !($_ -match "execute/progress") -and
            !($_ -match "ng.exe") -and
            !($_ -match "Driver Service") -and
            !($_ -match "No element found with strategy") -and
            $_ -match "(^\s+at|^\s+\.\.\.|^[^\s]|^\s+message =|^\s+type =|^\s+<|^\s+\+)" -and
            !($_ -match "Start class")
        ) {
            Write-Output $_
        }
    }
    # Free sources
    Close-Resources $jdkPath
}

# Convert html file
$reportFolder = $rootProj + '\reports'
$files = Get-Files $reportFolder ".report"
$files | ForEach-Object {
    if ($_ -ne $null) {
        $reportFile = Get-Item $_
        New-HTMLReport $_ $reportFile.Directory.FullName | Out-Null
    }
}

# Zip report
$isExist = Test-Path -Path "C:\Program Files\7-Zip\7z.exe" -PathType Leaf
if($isExist) {
    $zipFile = $rootProj + '\report.7z'
    $command = 'C:\Program` Files\7-Zip\7z.exe a -t7z "' + $zipFile + '" "' + $reportFolder + '"'
    Invoke-Expression $command
    Remove-Item -Path $reportFolder -Force -Recurse
    if(!(Test-Path $reportFolder)) {
        New-Item -Path $reportFolder -ItemType Directory | Out-Null
    }
    Move-Item -Path $zipFile -Destination $reportFolder
} else {
    $zipFile = $rootProj + '\report.zip'
    Compress-Archive -LiteralPath $reportFolder -DestinationPath $zipFile
    Remove-Item -Path $reportFolder -Force -Recurse
    if(!(Test-Path $reportFolder)) {
        New-Item -Path $reportFolder -ItemType Directory | Out-Null
    }
    Move-Item -Path $zipFile -Destination $reportFolder
}
