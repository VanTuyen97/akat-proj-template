# AkaAT Studio tool project template

## Gitlab Pipeline
- <details open>
    <summary>Windows PC configuration and registration with gitlab project</summary>

    - Step 1: Install AkaAT Agent Alpha ([Guideline install](#akaat-agent-alpha))
    - Step 2: Download all drivers for the browser you want to run the test, extract and save to a folder ([download link](#browser-driver)). Then add this folder to `Path` in `system variable` ([instruction](#tools-and-instruction))

    - <details>
        <summary>Step 3: On browsers, in gitlab page go to <code>Setting</code> => <code>CI/CD</code> => expand <code>Runners</code> => <code>Specific runners</code>.</summary>

        ![image](/resources/static_images/gitlab_cicd_page.png)
        </details>
    - Step 4: Donload [GitLab Runner](https://docs.gitlab.com/runner/install/) and extract to a folder.
    - <details>
        <summary>Step 5: Open cmd as administrator at the folder and run <code>gitlab-runner.exe register</code></summary>

        ![image](/resources/static_images/gitlab_register.png)
        - Enter Gitlab instance URL and registration token got in step 3.1
        - Description: type any
        - optional maintenance note for the runner: type any
        - Tags: this will be used in the gitlab pipeline to determine which runner should be used.
        - Execute: type `shell`
        </details>
    - <details>
        <summary>6: Open <code>config.toml</code> and edit</summary>

        ```toml
        concurrent = 1
        check_interval = 0

        [session_server]
        session_timeout = 1800

        [[runners]]
        name = "project runner"
        url = "https://gitlab.com/"
        token = "fdafhdkafhdiurweondsd"
        executor = "shell"
        shell = "powershell"         (1)
        [runners.custom_build_dir]
        [runners.cache]
            [runners.cache.s3]
            [runners.cache.gcs]
            [runners.cache.azure]
        ```
        - (1) The available shells can run on different platforms. [this](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-shells) is list shells hat gitlab supports.
        </details>
    - Step 7: run command `gitlab-runner.exe install`
    - Step 8: run command `gitlab-runner.exe start`
    - <details>
        <summary>Step 9: Refresh page in step 1 and check existing new runner and active.</summary>

        ![image](/resources/static_images/gitlab_runner_is_active.png)
        - <details>
            <summary><span style="color: red">Note:</span> if runner is not active (not visible circle green icon) => can try to open windows cmd as admin and run following command</summary>
            
            - `Gitlab-runner.exe stop`
            - `Gitlab-runner.exe uninstall`
            - `Gitlab-runner.exe install`
            - `Gitlab-runner.exe start`
            </details>
        </details>
    
    </details>

## Tools and Instruction
- <details open>
    <summary>Add a directory to the Path variable in the system variable</summary>

    - Step 1: Press windows key and input `system environment variables` and open `Edit the system environment variables`
    - <details>
        <summary>Step 3: Click <code>Environment variables</code></summary>
        
        ![image](/resources/static_images/System_properties.png)
        </details>

    - <details>
        <summary>Step 3: In <code>System variables</code>, edit variable <code>Path</code> variable</summary>
        
        ![image](/resources/static_images/edit_system_path_variable.png)
        </details>
    - <details>
        <summary>Step 4: Click <code>New</code> and type path</summary>
        
        ![image](/resources/static_images/add_folder_system_path.png)
        </details>
    - Step 5: Click OK three times to close all windows of `System properties`
    </details>
### AkaAT Agent Alpha
- <details open>
    <summary>Install on Windows</summary>

    - Step 1: Download the akaAT Studio V2 Alpha [here](https://akaat.com/download/startdownload) and install it.
    - Step 2: Open the tool and log in with the account of the website [akatester](https://akatester.com/)
    - <details>
        <summary>Step 3: Click on driver update icon => check browser and select <code>Download All</code></summary>

        ![image](/resources/static_images/download_driver.png)
        </details>
    - Step 4: Add `C:\Users\vantu\AppData\Local\Programs\akastudio\resources\agent-core` to the Path variable in the system variable, Follow the instructions [here](#tools-and-instruction)
    - Step 5: Close tool
    </details>
### Browser driver
You need to check your browser version and download the corresponding driver from the links below:
- Chrome driver donwload [here](https://chromedriver.chromium.org/downloads)
- Microsoft Edge driver donwload [here](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/)
- Firefox driver donwload [here](https://github.com/mozilla/geckodriver/releases)