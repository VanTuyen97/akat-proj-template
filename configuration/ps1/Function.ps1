# *** CONTANST
$currentDir = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
$currentDir = Get-Item $currentDir
$rootProj = $currentDir.Parent.Parent.FullName

$akatPath = [string](where.exe aka-rendezvous-1.0.1.jar)
$akatPath = $akatPath.Replace(" ", "`` ")
$akatDir = $akatPath -replace "aka-rendezvous-1.0.1.jar",""

# *** FUNCTION
function Get-Files($Dir, $Extension) {
    $reports = [System.Collections.ArrayList]::new()
    $isDir = (Get-Item $Dir) -is [System.IO.DirectoryInfo]
    if($isDir) {
        $files = Get-ChildItem -Path $Dir -File
        $files | Foreach-Object {
            if ($_.Name.endswith($Extension)) {
                [void]$reports.Add($_.FullName)
            }
        }
        $files = Get-ChildItem -Path $Dir -Directory
        $files | Foreach-Object {
            $subFiles = (Get-Files $_.FullName $Extension)
            $subFiles | Foreach-Object {
                [void]$reports.Add($_)
            }
        }
    } else {
        $file = Get-Item $Dir
        if ($file.Name.endswith($Extension)) {
            [void]$reports.Add($file.FullName)
        }
    }
    Write-Output $()$reports
}

function Get-Variable($Path) {
    $vars = [System.Collections.ArrayList]::new()
    foreach($line in [System.IO.File]::ReadLines($Path))
    {
        $line = $line.trim()
        if([string]::IsNullOrEmpty($line) -eq $False -and $line.Split("=")[0].StartsWith("#") -eq $False) {
            [void]$vars.Add(@{$line.Split("=")[0]=$line.Split("=")[1]})
        }
    }
    return $vars;
}

function Get-FileID($Path) {
    $uuid = ""
    foreach($line in [System.IO.File]::ReadLines($Path))
    {
        if($line -match "`"uuid`": `"([\w-]+)`"") {
            $uuid = $Matches.1
        }
    }
    return $uuid;
}

function Get-File-Test($Path) {
    $results = [System.Collections.ArrayList]::new()
    $files = Get-Files $Path ".suite"
    $files | Foreach-Object {
        if($_ -ne $null) {[void]$results.Add($_)}
    }
    $files = Get-Files $Path ".tc"
    $files | Foreach-Object {
        if($_ -ne $null) {[void]$results.Add($_)}
    }
    $files = Get-Files $Path ".plan"
    $files | Foreach-Object {
        if($_ -ne $null) {[void]$results.Add($_)}
    }
    Write-Output $()$results
}

function Close-Resources($jdkPath) {
    $commands = @(
    # "taskkill /f /im chrome.exe"
    # "taskkill /f /im firefox.exe"
    # "taskkill /f /im msedge.exe"
    "taskkill /f /im chromedriver.exe"
    "taskkill /f /im geckodriver.exe"
    "taskkill /f /im msedgedriver.exe")
    for ($idx = 0; $idx -lt $commands.length; $idx ++) {
        $commands[$idx]
        Invoke-Expression $commands[$idx]
    }
    Close-Task $jdkPath "aka-rendezvous-1.0.1.jar"
}

function Add-Vars($param) {
    $arg = $param -split ","
    $arg | Foreach-Object {
        $envVals = $rootProj + "\env.properties"
        Add-Content -Path $envVals -Value $_.trim()
    }
}

function Close-Task($jdkPath, $jarName) {
    $command = "$jdkPath\bin\jps"
    $output = Invoke-Expression $command
    $lines = $output.Split([Environment]::NewLine)
    $lines | Foreach-Object {
        $arr = $_.Split(" ")
        if($arr[1] -eq $jarName) {
            $msg = "Kill process " + $arr[0]
            Write-Output $msg
            Stop-process -Id $arr[0]
        }
    }
}

function Copy-Capability($from, $to) {
    if($to.EndsWith("\")) {
        $to = $to.Substring(0,$to.length)
    }
    $files = Get-Files $from ".capability"
    $files | ForEach-Object {
        if ($_ -ne $null) {
            $relPath = $_.Substring($from.length)
            if($relPath.StartsWith("\")) {
                $relPath = $relPath.Substring(1)
            }
            Copy-Item -Path $_ -Destination "$to\$relPath" -Force
        }
    }
}

function New-HTMLReport($reportPath, $htmlFolder) {
    $reportFile = Get-Item $reportPath
    $reportName = $reportFile.Name -replace ".report",""
    # Create html folder
    $htmlFolder = Get-Item $htmlFolder
    $htmlFolder = Join-Path -Path $htmlFolder.FullName -ChildPath $reportName
    if(Test-Path $htmlFolder) {
        Remove-Item -Path $htmlFolder -Recurse -Force -Confirm:$false | Out-Null
    }
    New-Item -Path $htmlFolder -ItemType Directory | Out-Null
    # Extract report template
    $template = $akatDir + "templates\ReportTemplate.zip"
    Expand-Archive -Path $template -DestinationPath $htmlFolder
    # Create data folder
    New-Item -Path "$htmlFolder\data" -ItemType Directory | Out-Null
    New-Item -Path "$htmlFolder\data\report.js" -ItemType File | Out-Null
    "const report = " | Out-File -FilePath "$htmlFolder\data\report.js" -Append -NoNewline 
    $reader = New-Object -TypeName System.IO.StreamReader -ArgumentList $reportPath
    $uuid = $Null
    while($true) {
        $arrayChar = New-Object -TypeName char[] -ArgumentList 600
        $readResult = $reader.ReadBlockAsync($arrayChar, 0, 600)
        $size = $readResult.Result - 1
        $str = $arrayChar[0..$size] -join ""
        if($readResult.Result -eq 0) {
            break;
        }
        if(($Null -eq $uuid) -and ($str -match "uuid`":`"([\w\d-]+)`"")) {
            $uuid = $matches.1
        }
        $str | Out-File -FilePath "$htmlFolder\data\report.js" -Append -NoNewline 
    }
    $reader.Close()
    $reader.Dispose()
    # Create Image folder
    New-Item -Path "$htmlFolder\image" -ItemType Directory | Out-Null
    $imagePath = $reportFile.Directory.FullName + "\image\" + $uuid + "\*"
    if(Test-Path $imagePath) {
        Move-Item -Path $imagePath -Destination "$htmlFolder\image" -Force 
    }
    Move-Item -Path $reportPath -Destination "$htmlFolder" -Force 
    $result = $htmlFolder
    return $result    
}